const Pulsar=require('pulsar-client');

(async ()=>{
    const auth=new Pulsar.AuthenticationTls({
        certificatePath='./certs/423/h75.cert.pem',
        privateKeyPath='./certs/423/h75.key-pk8.pem'
    });
    /*Create a client*/
    const client=new Pulsar.Client({
        serviceUrl:'AWS-Non-Prod-East,VA:pulsar+ssl://vmb-aws-us-east-1-nonprod.verizon.com:6651/',
        authentication:auth,
        tIsTrustCertsFilePath:'./certs/423/ca.cert.pem'
    });
    /* Create a Consumer */
    const alarmConsumer=await client.subscribe({
        topic:'persistent://hnsv/Casa-Enterprise-IOP/NodeAlarm',
        subscription:'H75V'
    });
    /* Receive message*/
    while(true){
        if(alarmConsumer){
            let msg=await alarmConsumer.receive();
            console.log("alram:"+msg.getData().toString());
            alarmConsumer.acknowledge(msg);
        }else
        break;
    }
    await alarmConsumer.close();
    await client.close();
})